// Get Post Data

fetch('https://jsonplaceholder.typicode.com/posts')
.then((res)=> res.json())
.then((data) => showPosts(data));

// Show posts
const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post)=> {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>

			</div>

		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;

}

// add post

document.querySelector('#form-add-post').addEventListener('submit', (e)=>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST', 
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		//charset set to UTF-8 to accept other characters from different devices
		headers: {'Content-type' : 'application/json; charset=UTF-8'}


	}).then((res) => res.json())
	.then((data) => {
		console.log(data);
		alert("Successfully added");
		// empties the input fields
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
})

// edit post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
}

// delete post

const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: 'DELETE'});
	// getelemtnbyid does not need #
	document.getElementById(`post-${id}`).remove();
}

// Update post

document.getElementById('form-edit-post').addEventListener('submit', (e)=>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {

		method: 'PUT',
		body: JSON.stringify({
			id: document.getElementById('txt-edit-id'),
			title: document.getElementById('txt-edit-title'),
			body: document.getElementById('txt-edit-body'),
			userId: 1

		}),

		headers: {'Content-type' : 'application/json; charset=UTF-8'}
	}).then((res) => res.json())
	.then((data) => {
		console.log(data);
		alert("Successfully updated");

		document.getElementById('txt-edit-id').value = null;
		document.getElementById('txt-edit-title').value = null;
		document.getElementById('txt-edit-body').value = null;
		document.getElementById('btn-submit-update').setAttribute('disabled', true);

	})
})